# ldap-account-manager
A simple interface to manage your LDAP accounts written with python and flask.
Features:

- Log-in with a LDAP account;
- Edit your profile;
- Define 'manager' groups that can edit other users accounts;
- Sends password reset link by email when password are forgotten.

# Development

You may need to install the python-poetry of your system:

```bash
poetry install
```

Launch the development server:
```bash
cp config.sample.toml config.toml
env FLASK_APP=web FLASK_ENV=development poetry run flask run
```
