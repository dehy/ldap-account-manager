import email.message
import hashlib
import smtplib
import wtforms
import wtforms.validators
from flask import (
    Blueprint,
    render_template,
    request,
    flash,
    redirect,
    url_for,
    session,
    g,
)
import web.ldaputils
import web.form

app = Blueprint("public", __name__, url_prefix="/")


@app.route("/")
def index():
    if "user" in session:
        return redirect(url_for("account.user", user_id=session["user"]))

    return redirect(url_for("public.login"))


class LoginForm(web.form.Form):
    login = wtforms.StringField(
        "Identifiant",
        validators=[wtforms.validators.DataRequired()],
        render_kw={"placeholder": "mdupont"},
    )
    password = wtforms.PasswordField(
        "Mot de passe", validators=[wtforms.validators.DataRequired()]
    )


@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)

    if request.form:
        if not form.validate():
            flash("Impossible de vous connecter, vérifiez vos identifiants", "error")
        else:
            user_id = web.ldaputils.authenticate(
                request.form["login"], request.form["password"]
            )
            if not user_id:
                flash(
                    "Impossible de vous connecter, vérifiez vos identifiants", "error"
                )
            else:
                flash("Identification réussie", "success")
                session["user"] = user_id
                session["is_manager"] = web.ldaputils.is_manager(user_id)
                return redirect(url_for("account.user", user_id=session["user"]))

    return render_template("login.html", form=form)


@app.route("/logout")
def logout():
    try:
        del session["user"]
    except KeyError:
        pass

    try:
        del session["is_manager"]
    except KeyError:
        pass

    flash("Vous êtes déconnectés", "success")
    return redirect(url_for("public.index"))


class ForgottenPasswordForm(web.form.Form):
    login = wtforms.StringField(
        "Identifiant ou email",
        validators=[wtforms.validators.DataRequired()],
        render_kw={"placeholder": "mdupont@exemple.com"},
    )


def profile_hash(user, password):
    return hashlib.sha256(
        g.settings["secret"].encode("utf-8")
        + user.encode("utf-8")
        + password.encode("utf-8")
    ).hexdigest()


@app.route("/reset", methods=["GET", "POST"])
def forgotten():
    form = ForgottenPasswordForm(request.form)
    if not request.form:
        return render_template("passwordforgotten.html", form=form)

    if not form.validate():
        flash("Impossible de réinitialiser votre mot de passe.", "error")
        return render_template("passwordforgotten.html", form=form)

    profile = web.ldaputils.get_profile(request.form["login"])
    if not profile:
        flash("Le lien de réinitialisation vous a été envoyé.", "success")
        return render_template("passwordforgotten.html", form=form)

    recipient = profile["mail"]
    subject = "Réinitialisation de votre mot de passe sur {}".format(
        g.settings["website_name"]
    )
    url = g.settings.get("website_url", "http://localhost:5000") + url_for(
        "public.reset",
        uid=profile["uid"],
        hash=profile_hash(profile["uid"], profile["userPassword"]),
    )
    text_body = "Pour ré-initialiser le mot de passe de votre compte {}, cliquez sur le lien suivant :\n{}".format(
        g.settings["website_name"], url
    )

    try:
        msg = email.message.EmailMessage()
        msg.set_content(text_body)
        msg["Subject"] = subject
        msg["From"] = g.settings["smtp"]["from_addr"]
        msg["To"] = recipient

        with smtplib.SMTP(
            host=g.settings["smtp"]["host"], port=g.settings["smtp"]["port"]
        ) as smtp:
            if g.settings["smtp"].get("tls"):
                smtp.starttls()
            if g.settings["smtp"].get("login"):
                smtp.login(
                    user=g.settings["smtp"]["login"],
                    password=g.settings["smtp"].get("password"),
                )
            smtp.send_message(msg)

    except:
        flash("Impossible de réinitialiser votre mot de passe.", "error")

    else:
        flash("Le lien de réinitialisation vous a été envoyé.", "success")

    return render_template("passwordforgotten.html", form=form)


class PasswordResetForm(web.form.Form):
    password = wtforms.PasswordField(
        "Mot de passe", validators=[wtforms.validators.DataRequired()]
    )
    confirmation = wtforms.PasswordField(
        "Confirmation du mot de passe",
        validators=[
            wtforms.validators.EqualTo(
                "password", "Les deux mots de passe sont différents."
            ),
        ],
    )


@app.route("/reset/<uid>/<hash>", methods=["GET", "POST"])
def reset(uid, hash):
    form = PasswordResetForm(request.form)

    profile = web.ldaputils.get_profile(uid)
    if not profile or hash != profile_hash(profile["uid"], profile["userPassword"]):
        flash(
            "Le lien de réinitialisation qui vous a amené sur ce site est invalide.",
            "error",
        )
        return redirect(url_for("public.index"))

    if request.form and form.validate():
        dn = "cn={cn},{base}".format(
            cn=profile["cn"], base=g.settings["ldap"]["users_base_dn"],
        )
        web.ldaputils.set_password(dn, request.form["password"])
        session["user"] = uid
        session["is_manager"] = web.ldaputils.is_manager(uid)

        flash("Votre mot de passe a correctement été mis-à-jour", "success")
        return redirect(url_for("account.user", user_id=uid))

    return render_template("passwordreset.html", form=form, uid=uid, hash=hash)
