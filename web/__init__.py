import flask
import ldap
import logging
import logging.config
import logging.handlers
import toml
import os

import web.public
import web.account

app = None


def create_app():
    settings = toml.load(os.environ.get("CONFIG", "config.toml"))

    try:
        logging.config.fileConfig("logs.ini")
    except KeyError:
        pass

    app = flask.Flask(__name__)
    app.url_map.strict_slashes = False
    app.secret_key = settings["secret"]

    @app.before_request
    def before_request():
        flask.g.settings = settings
        flask.g.ldap = ldap.initialize(flask.g.settings["ldap"]["uri"])
        flask.g.ldap.simple_bind_s(
            flask.g.settings["ldap"]["user_dn"], flask.g.settings["ldap"]["password"],
        )

    @app.after_request
    def after_request(response):
        if "ldap" in flask.g:
            flask.g.ldap.unbind_s()
        return response

    @app.context_processor
    def global_processor():
        return {
            "user": flask.session.get("user"),
            "is_manager": flask.session.get("is_manager"),
            "logo_url": settings["logo_url"],
            "website_name": settings["website_name"],
        }

    @app.errorhandler(404)
    def page_not_found(e):
        return flask.render_template("error.html", error=404), 404

    app.register_blueprint(web.public.app)
    app.register_blueprint(web.account.app)

    return app
