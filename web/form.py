import wtforms


class Form(wtforms.Form):
    class Meta:
        locales = ["fr_FR", "fr"]
