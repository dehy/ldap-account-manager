from flask import (
    Blueprint,
    render_template,
    flash,
    request,
    g,
    url_for,
    session,
    redirect,
)
import wtforms
import wtforms.fields.html5
import wtforms.validators
import web.ldaputils


app = Blueprint("account", __name__, url_prefix="/account")


class BaseProfileForm(web.form.Form):
    login = wtforms.TextField(
        "Identifiant",
        validators=[wtforms.validators.DataRequired()],
        render_kw={"placeholder": "mdupont"},
    )

    first_name = wtforms.TextField(
        "Prénom",
        validators=[wtforms.validators.DataRequired()],
        render_kw={"placeholder": "Marie"},
    )
    last_name = wtforms.TextField(
        "Nom de famille",
        validators=[wtforms.validators.DataRequired()],
        render_kw={"placeholder": "Dupont"},
    )
    email = wtforms.fields.html5.EmailField(
        "Adresse email",
        validators=[wtforms.validators.DataRequired(), wtforms.validators.Email()],
        render_kw={"placeholder": "marie.dupont@mdupont.fr"},
    )
    password1 = wtforms.PasswordField(
        "Mot de passe",
        validators=[wtforms.validators.Optional(), wtforms.validators.Length(min=8)],
    )
    password2 = wtforms.PasswordField(
        "Confirmation du mot de passe",
        validators=[
            wtforms.validators.Optional(),
            wtforms.validators.EqualTo(
                "password1", "Les deux mots de passe sont différents."
            ),
        ],
    )


class AdminProfileForm(BaseProfileForm):
    groups = wtforms.SelectMultipleField("Groupes")


@app.route("/")
def users():
    users = web.ldaputils.get_users()
    return render_template("users.html", users=users)


@app.route("/edit/<user_id>", methods=["GET", "POST"])
def user(user_id):
    if not session.get("user") or (
        session["user"] != user_id and not session["is_manager"]
    ):
        return render_template("error.html", error=400), 400

    profile = web.ldaputils.get_profile(user_id)

    if not profile:
        return render_template("error.html", error=404), 404

    form_class = AdminProfileForm if session["is_manager"] else BaseProfileForm

    ldap_form = {
        "uid": "login",
        "givenName": "first_name",
        "sn": "last_name",
        "mail": "email",
    }

    default_values = {
        form_key: profile[ldap_key] for ldap_key, form_key in ldap_form.items()
    }

    if session["is_manager"]:
        default_values["groups"] = web.ldaputils.groups_for(user_id)

    form = form_class(request.form or None, data=default_values)
    form.login.render_kw = {"readonly": True, "placeholder": "mdupont"}
    form.action = url_for("account.user", user_id=user_id)

    if session["is_manager"]:
        form.groups.choices = [(group, group) for group in web.ldaputils.get_groups()]

    if request.form:
        dn = "cn={cn},{base}".format(
            cn=profile["cn"], base=g.settings["ldap"]["users_base_dn"],
        )

        ldap_keys = {
            ldap_key: request.form[form_key] for ldap_key, form_key in ldap_form.items()
        }

        if not form.validate():
            flash(
                "Le profil n'a pas été mis à jour. "
                "Veuillez vérifier vos informations",
                "error",
            )

        elif not web.ldaputils.set_profile(dn, ldap_keys) or (
            "groups" in request.form
            and not web.ldaputils.update_groups(user_id, request.form.getlist("groups"))
        ):
            flash(
                "Le profil n'a pas été mis à jour. "
                "Une erreur technique est survenue.",
                "error",
            )

        elif request.form["password1"] and not web.ldaputils.set_password(
            dn, request.form["password1"]
        ):
            flash(
                "Le mot de passe n'a pas été mis à jour. "
                "Une erreur technique est survenue.",
                "error",
            )

        else:
            flash("Le profil a été mis à jour", "success")

    return render_template("profile.html", form=form)


@app.route("/add", methods=["GET", "POST"])
def user_add():
    if not session.get("is_manager"):
        return "Accèse interdit", 400

    ldap_form = {
        "uid": "login",
        "givenName": "first_name",
        "sn": "last_name",
        "mail": "email",
    }

    default_values = {"groups": ["cooperators"]}

    form = AdminProfileForm(request.form or None, data=default_values)
    form.action = url_for("account.user_add")
    form.groups.choices = [(group, group) for group in web.ldaputils.get_groups()]

    if request.form:
        ldap_keys = {
            ldap_key: request.form[form_key] for ldap_key, form_key in ldap_form.items()
        }
        ldap_keys["cn"] = "{} {}".format(
            request.form["first_name"], request.form["last_name"]
        )

        dn = "cn={cn},{base}".format(
            cn=ldap_keys["cn"], base=g.settings["ldap"]["users_base_dn"]
        )

        if not form.validate():
            flash(
                "Le profil n'a pas été créé. " "Veuillez vérifier vos informations",
                "error",
            )

        elif not web.ldaputils.add_profile(dn, ldap_keys) or (
            "groups" in request.form
            and not web.ldaputils.update_groups(
                request.form["login"], request.form.getlist("groups")
            )
        ):
            flash(
                "Le profil n'a pas été créé. "
                "Soit il existe déjà, soit une erreur technique est survenue.",
                "error",
            )

        elif request.form["password1"] and not web.ldaputils.set_password(
            dn, request.form["password1"]
        ):
            flash(
                "Le mot de passe n'a pas été créé. "
                "Une erreur technique est survenue.",
                "error",
            )

        else:
            flash("Le profil a été créé", "success")
            return redirect(url_for("account.user_add"))

    return render_template("profile.html", form=form)


@app.route("/del/<user_id>")
def user_del(user_id):
    profile = web.ldaputils.get_profile(user_id)
    dn = "cn={cn},{base}".format(
        cn=profile["cn"], base=g.settings["ldap"]["users_base_dn"],
    )

    web.ldaputils.update_groups(dn, [])
    if web.ldaputils.del_profile(dn):
        flash("Le compte a été supprimé", "success")
    else:
        flash("Le compte n'a pas été supprimé", "error")

    return redirect(url_for("account.users"))
