import flask
import ldap


def authenticate(login, password):
    try:
        result = flask.g.ldap.search_s(
            flask.g.settings["ldap"]["users_base_dn"],
            ldap.SCOPE_SUBTREE,
            flask.g.settings["ldap"]["users_filter"].format(login=login),
        )

        if not result:
            return None

        user_dn = result[0][0]
        flask.g.ldap.simple_bind_s(
            user_dn, password,
        )

        user_id = result[0][1]["uid"][0].decode("utf-8")

    except ldap.LDAPError:
        return None

    return user_id


def is_manager(user_id):
    try:
        result = flask.g.ldap.search_s(
            flask.g.settings["ldap"]["groups_base_dn"],
            ldap.SCOPE_SUBTREE,
            flask.g.settings["ldap"]["manager_filter"].format(uid=user_id),
        )

        return bool(result)

    except ldap.LDAPError:
        return False


def get_profile(user_id):
    try:
        result = flask.g.ldap.search_s(
            flask.g.settings["ldap"]["users_base_dn"],
            ldap.SCOPE_SUBTREE,
            flask.g.settings["ldap"]["users_filter"].format(login=user_id),
        )

        if not result:
            return None

        return {key: value[0].decode("utf-8") for key, value in result[0][1].items()}

    except ldap.LDAPError:
        return None


def set_profile(user_dn, attributes):
    attributes = [
        (ldap.MOD_REPLACE, key, value.encode("utf-8"))
        for key, value in attributes.items()
    ]

    try:
        flask.g.ldap.modify_s(user_dn, attributes)

    except ldap.LDAPError:
        return False

    return True


def add_profile(user_dn, attributes):
    attributes = [(key, value.encode("utf-8")) for key, value in attributes.items()]
    attributes.append(("objectClass", [b"inetOrgPerson", b"top"]))

    try:
        flask.g.ldap.add_s(user_dn, attributes)

    except ldap.LDAPError:
        return False

    return True


def del_profile(user_dn):
    try:
        flask.g.ldap.delete_s(user_dn)

    except ldap.LDAPError:
        return False

    return True


def set_password(user_dn, new_password):
    try:
        flask.g.ldap.passwd_s(
            user_dn.encode("utf-8"), None, new_password.encode("utf-8"),
        )

    except ldap.LDAPError:
        return False

    return True


def get_users():
    try:
        entries = flask.g.ldap.search_s(
            flask.g.settings["ldap"]["users_base_dn"],
            ldap.SCOPE_SUBTREE,
            "(objectClass=inetOrgPerson)",
        )

        if not entries:
            return []

        return [
            {key: value[0].decode("utf-8") for key, value in entry.items()}
            for _, entry in entries
        ]

    except ldap.LDAPError:
        return None


def get_groups():
    try:
        entries = flask.g.ldap.search_s(
            flask.g.settings["ldap"]["groups_base_dn"],
            ldap.SCOPE_SUBTREE,
            "(objectClass=posixGroup)",
        )

        if not entries:
            return []

        return [entry[1]["cn"][0].decode("utf-8") for entry in entries]

    except ldap.LDAPError:
        return None


def groups_for(user_id):
    try:
        entries = flask.g.ldap.search_s(
            flask.g.settings["ldap"]["groups_base_dn"],
            ldap.SCOPE_SUBTREE,
            "(&(objectClass=posixGroup)(memberUid={uid}))".format(uid=user_id),
        )

        if not entries:
            return []

        return [entry[1]["cn"][0].decode("utf-8") for entry in entries]

    except ldap.LDAPError:
        return None


def update_groups(user_id, groups):
    new_groups = set(groups)
    old_groups = set(groups_for(user_id))
    del_groups = list(old_groups - new_groups)
    add_groups = list(new_groups - old_groups)

    try:
        for group in add_groups:
            dn = "cn={group},{base}".format(
                group=group, base=flask.g.settings["ldap"]["groups_base_dn"]
            )
            flask.g.ldap.modify_s(
                dn, [(ldap.MOD_ADD, "memberUid", user_id.encode("utf-8"))]
            )

        for group in del_groups:
            dn = "cn={group},{base}".format(
                group=group, base=flask.g.settings["ldap"]["groups_base_dn"]
            )
            flask.g.ldap.modify_s(
                dn, [(ldap.MOD_DELETE, "memberUid", user_id.encode("utf-8"))]
            )

        return True

    except ldap.LDAPError:
        return False
